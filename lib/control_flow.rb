# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  characters = str.chars
  characters.each do |char|
    if char == char.downcase
      characters.delete(char)
    end
  end
  characters.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str_length = str.length
  mid_index = str_length/2
  if str_length.even?
    str[mid_index-1..mid_index]
  else
    str[mid_index]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count("aeiou")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  count = 1
  while count <= num
    product *= count
    count += 1
  end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  string = ""
  count = 0
  while count < arr.length
    count != arr.length - 1 ? string += "#{arr[count]}#{separator}" : string += "#{arr[count]}"
    count += 1
  end
  string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_string = ""
  count = 0
  while count < str.length
    if count.even?
      new_string << str[count].downcase
    else
      new_string << str[count].upcase
    end
    count += 1
  end
  new_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(" ")
  reversed = words.map do |word|
    word.length > 4 ? word.reverse : word
  end
  reversed.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def multiple_of_3(int)
  int % 3 == 0 ? "fizz" : ""
end

def multiple_of_5(int)
  int % 5 == 0 ? "buzz" : ""
end

def fizzbuzz(n)
  arr = []
  count = 1
  while count <= n
    fizzbuzz = ""
    fizzbuzz << multiple_of_3(count)
    fizzbuzz << multiple_of_5(count)
    fizzbuzz.empty? ? arr << count : arr << fizzbuzz
    count += 1
  end

  arr
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse_arr = []
  arr.reverse_each do |el|
    reverse_arr << el
  end

  reverse_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num < 2
    return false
  end

  count = 2
  while count < num
    break if count == num
    if num % count == 0
      return false
    end
    count += 1
  end

  true
end
#puts prime?(2)

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr_of_factors = []
  factor = 1
  while factor <= num
    arr_of_factors << factor if num % factor == 0
    factor += 1
  end
  arr_of_factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  all_factors = factors(num)
  primes = all_factors.select do |factor|
    prime?(factor)
  end
  primes
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even, odd = [], []
  arr.each do |n|
    even << n if n.even?
    odd << n if n.odd?
  end
  even.length > odd.length ? odd[0] : even[0]
end
